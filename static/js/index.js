// MUDANDO LOGO CONFORME O
// TAMANHO DA TELA
window.addEventListener("resize", alturaDIV);

function alturaDIV() {
    var largura = $('#principal').width();
    
    if ( largura > "762" ) {
        $("#dotjunior img").attr("src","static/images/dotjunior-logo-extendido.png");
    }   
   
    if ( largura < "762" ) {
        $("#dotjunior img").attr("src","static/images/dotjunior-logo.png");
    }
    
    if ( largura < "480" ) {
        $("#dotjunior img").attr("src","static/images/dotjunior-icone.png");
    }
}

alturaDIV();
