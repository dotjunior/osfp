from flask import Flask, render_template

import os

application = Flask(__name__)

@application.route("/")
def home():
    return render_template('index.html')

# Remove the "#" from below lines, and access /Py1Nf0 for view OS ENV.
#@application.route("/Py1Nf0")
#def pyinfo():
#    return render_template('Py1Nf0.html', osenv = os.environ)

if __name__ == "__main__":
    application.run()
